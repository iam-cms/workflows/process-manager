#include "engine_cli.hpp"

#include "config.hpp"
#include "logging.hpp"
#include "process.hpp"
#include "utility.hpp"

#include <nlohmann/json.hpp>
#include <spdlog/spdlog.h>

#include <iostream>

using namespace std::chrono_literals;

namespace {

  using namespace process_manager;

  process::Result exec_cmd(const std::vector<std::string> &args) {
    std::stop_source stop_source{};
    const auto result = process::run(args, 2000ms, stop_source.get_token());
    if (result.error_code) {
      logging::default_logger()->error("Could not execute '{}'. Reason: {}", args[0], result.error_code.message());
    }

    return result;
  }
} // namespace

namespace process_manager::engine_cli {
  bool run(const std::filesystem::path &engine_path, const std::filesystem::path &engine_working_dir, const std::filesystem::path &workflow_file, bool no_color) noexcept {
    std::vector<std::string> args{};
    args.emplace_back(engine_path.string());
    args.emplace_back("run");

    if (no_color) {
      args.emplace_back("--no-color");
    }

    args.emplace_back("-p");
    args.emplace_back(engine_working_dir.string());
    args.emplace_back(workflow_file.string());

    return process::run_detached(args);
  }

  bool resume(const std::filesystem::path &engine_path, const std::filesystem::path &engine_working_dir) {
    const auto result = exec_cmd({engine_path.string(), "continue", "-p", engine_working_dir.string()});
    return !result.error_code;
  }

  bool cancel(const std::filesystem::path &engine_path, const std::filesystem::path &engine_working_dir) {
    const auto result = exec_cmd({engine_path.string(), "cancel", "-p", engine_working_dir.string()});
    return !result.error_code;
  }

  std::optional<std::string> log(const std::filesystem::path &engine_path, const std::filesystem::path &engine_working_dir) noexcept {
    const auto result = exec_cmd({engine_path.string(), "log", "-p", engine_working_dir.string()});
    return result.error_code ? std::nullopt : std::optional<std::string>{result.out};
  }

  std::optional<std::string> log_path(const std::filesystem::path &engine_path, const std::filesystem::path &engine_working_dir) noexcept {
    const auto result = exec_cmd({engine_path.string(), "log_path", "-p", engine_working_dir.string()});
    return result.error_code ? std::nullopt : std::optional<std::string>{result.out};
  }

  std::optional<std::string> tree(const std::filesystem::path &engine_path, const std::filesystem::path &engine_working_dir) noexcept {
    const auto result = exec_cmd({engine_path.string(), "tree", "-p", engine_working_dir.string()});
    return result.error_code ? std::nullopt : std::optional<std::string>{result.out};
  }

  std::optional<std::string> tree_path(const std::filesystem::path &engine_path, const std::filesystem::path &engine_working_dir) noexcept {
    const auto result = exec_cmd({engine_path.string(), "tree_path", "-p", engine_working_dir.string()});
    return result.error_code ? std::nullopt : std::optional<std::string>{result.out};
  }

  std::optional<std::string> interactions(const std::filesystem::path &engine_path, const std::filesystem::path &engine_working_dir) noexcept {
    const auto result = exec_cmd({engine_path.string(), "interactions", "-p", engine_working_dir.string()});
    return result.error_code ? std::nullopt : std::optional<std::string>{result.out};
  }

  bool input(const std::filesystem::path &engine_path, const std::filesystem::path &engine_working_dir, std::string interaction_id, std::string value) noexcept {
    const auto result = exec_cmd(
      {
        engine_path.string(),
        "input",
        "-p",
        engine_working_dir.string(),
        std::move(interaction_id),
        std::move(value),
      });

    return !result.error_code;
  }

  std::optional<std::string> status(const std::filesystem::path &engine_path, const std::filesystem::path &engine_working_dir) noexcept {
    const auto result = exec_cmd({engine_path.string(), "status", "-p", engine_working_dir.string()});
    return result.error_code ? std::nullopt : std::optional<std::string>{result.out};
  }

  std::optional<std::string> artifacts(const std::filesystem::path &engine_path, const std::filesystem::path &engine_working_dir) noexcept {
    const auto result = exec_cmd({engine_path.string(), "shortcuts", "-p", engine_working_dir.string()});
    return result.error_code ? std::nullopt : std::optional<std::string>{result.out};
  }
} // namespace process_manager::engine_cli