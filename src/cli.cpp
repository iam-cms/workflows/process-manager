#include "cli.hpp"

#include "config.hpp"
#include "engine_cli.hpp"
#include "logging.hpp"
#include "manager.hpp"
#include "utility.hpp"

#include <nlohmann/json.hpp>
#include <spdlog/spdlog.h>

#include <chrono>
#include <thread>

using namespace std::chrono_literals;

namespace {
  using namespace process_manager;
  using namespace process_manager::manager;

  [[nodiscard]] std::optional<std::tuple<Engine, Workflow>> prepare_engine_call(const std::filesystem::path &manager_working_dir, int workflow_id) noexcept {
    auto logger = logging::default_logger();

    auto db = manager::open_db(manager_working_dir);
    if (!db) {
      logger->critical("Could not open database in '{}'", manager_working_dir.string());
      return std::nullopt;
    }

    auto workflow = db->get_workflow_by_id(workflow_id);
    if (!workflow) {
      logger->error("Could not find workflow '{}'", workflow_id);
      return std::nullopt;
    }

    const auto loaded_engines = manager::load_engines(manager_working_dir / config::DEFAULT_ENGINE_FILENAME);
    if (!loaded_engines) {
      logger->error("Could not load engines from '{}'", manager_working_dir.string());
      return std::nullopt;
    }

    auto engine = manager::find_engine_by_name(loaded_engines->first, workflow->engine);
    if (!engine) {
      logger->error("No suitable engine found");
      return std::nullopt;
    }

    return std::tuple<Engine, Workflow>{std::move(engine.value()), std::move(workflow.value())};
  }
} // namespace

namespace process_manager::cli {

  void register_commmon_options(CLI::App *parent, CommonOptions &options) noexcept {
    parent->add_option("-p,--path", options.working_dir, "Base directory to execute in (application needs write permissions here). Defaults to $HOME/.process_manager");
    parent->require_subcommand(1);
  }

  CLI::App *register_run_workflow(CLI::App *parent, RunOptions &options) noexcept {
    auto cmd = parent->add_subcommand("run");
    cmd->alias("start");

    cmd->add_option("-e,--engine", options.engine, "Workflow engine for processing. Use --list-engines for a list of available engines");
    cmd->add_flag("-C,--no-color", options.no_color, "Disable terminal colors");
    cmd->add_option("file", options.workflow_file, "Workflow description file (.flow)")->required();

    return cmd;
  }

  bool run_workflow(const std::filesystem::path &manager_working_dir, const RunOptions &options) noexcept {
    auto logger = logging::default_logger();

    auto db = manager::open_db(manager_working_dir);
    if (!db) {
      logger->critical("Could not open database in '{}'", manager_working_dir.string());
      return false;
    }

    const auto engines_and_default = manager::load_engines(manager_working_dir / config::DEFAULT_ENGINE_FILENAME);
    if (!engines_and_default) {
      logger->error("Could not load engines from '{}'", manager_working_dir.string());
      return false;
    }

    const auto engine = manager::select_engine(engines_and_default->first, options.engine, engines_and_default->second);
    if (!engine) {
      logger->error("No suitable engine found");
      return false;
    }

    const auto workflow = db->insert_workflow(engine->name, options.workflow_file);
    if (!workflow) {
      logger->error("Could not insert workflow");
      return false;
    }

    const auto engine_working_dir = manager::setup_engine_working_dir(manager_working_dir, workflow->id);
    if (!engine_working_dir) {
      logger->error("Could not create working directory for workflow");
      return false;
    }

    const auto start_result = engine_cli::run(engine->path, engine_working_dir.value(), options.workflow_file, options.no_color);
    if (!start_result) {
      logger->error("Failed to start engine");
      return false;
    }

    // Give the engine some time to startup.
    std::this_thread::sleep_for(100ms);

    const auto updated_workflow = manager::update_workflow(workflow->id, engine->path, engine_working_dir.value(), db.value());

    if (updated_workflow) {
      std::cout << nlohmann::json(updated_workflow.value()).dump(2) << std::endl;
    } else {
      logger->error("Could not get initial status of engine");
    }

    return true;
  }

  CLI::App *register_cancel_workflow(CLI::App *parent, CancelOptions &options) noexcept {
    auto cmd = parent->add_subcommand("cancel", "Cancel execution of a workflow with given ID");
    cmd->add_option("id", options.workflow_id, "ID of the workflow execution")->required();

    return cmd;
  }

  bool cancel_workflow(const std::filesystem::path &manager_working_dir, const CancelOptions &options) noexcept {
    const auto result = prepare_engine_call(manager_working_dir, options.workflow_id);
    if (!result) {
      return false;
    }

    const auto [engine, workflow] = result.value();

    return engine_cli::cancel(engine.path, manager::engine_working_dir(manager_working_dir, workflow.id));
  }

  CLI::App *register_continue_workflow(CLI::App *parent, ContinueOptions &options) noexcept {
    auto cmd = parent->add_subcommand("continue", "Continue execution of a workflow with given id");
    cmd->add_option("id", options.workflow_id, "ID of the workflow execution")->required();

    return cmd;
  }

  bool continue_workflow(const std::filesystem::path &manager_working_dir, const ContinueOptions &options) noexcept {
    const auto result = prepare_engine_call(manager_working_dir, options.workflow_id);
    if (!result) {
      return false;
    }

    const auto [engine, workflow] = result.value();

    return engine_cli::resume(engine.path, manager::engine_working_dir(manager_working_dir, workflow.id));
  }

  CLI::App *register_list_interactions(CLI::App *parent, InteractionOptions &options) noexcept {
    auto cmd = parent->add_subcommand("interactions", "Get interaction descriptions for given workflow execution");
    cmd->add_option("id", options.workflow_id, "ID of the workflow execution")->required();

    return cmd;
  }

  bool list_interactions(const std::filesystem::path &manager_working_dir, const InteractionOptions &options) noexcept {
    const auto result = prepare_engine_call(manager_working_dir, options.workflow_id);
    if (!result) {
      return false;
    }

    const auto [engine, workflow] = result.value();

    const auto fetch_result = engine_cli::interactions(engine.path, manager::engine_working_dir(manager_working_dir, workflow.id));
    if (fetch_result) {
      std::cout << fetch_result.value() << '\n';
    }

    return fetch_result.has_value();
  }

  CLI::App *register_add_input(CLI::App *parent, InputOptions &options) noexcept {
    auto cmd = parent->add_subcommand("input", "Provide input value for requested user interaction to the workflow engine");
    cmd->add_option("workflowId", options.workflow_id, "Id of the workflow execution")->required();
    cmd->add_option("interactionId", options.interaction_id, "Id of the interaction")->required();
    cmd->add_option("value", options.value, "Value")->required();

    return cmd;
  }

  bool add_input(const std::filesystem::path &manager_working_dir, const InputOptions &options) noexcept {
    const auto result = prepare_engine_call(manager_working_dir, options.workflow_id);
    if (!result) {
      return false;
    }

    const auto [engine, workflow] = result.value();

    std::string value = std::move(options.value);
    return engine_cli::input(engine.path, manager::engine_working_dir(manager_working_dir, workflow.id), options.interaction_id, std::move(value));
  }

  CLI::App *register_workflow_log(CLI::App *parent, LogOptions &options) noexcept {
    auto cmd = parent->add_subcommand("log", "Get the log of given workflow execution");
    cmd->add_option("id", options.workflow_id, "ID of the workflow execution")->required();

    return cmd;
  }

  bool workflow_log(const std::filesystem::path &manager_working_dir, const LogOptions &options) noexcept {
    const auto result = prepare_engine_call(manager_working_dir, options.workflow_id);
    if (!result) {
      return false;
    }

    const auto [engine, workflow] = result.value();

    const auto fetch_result = engine_cli::log(engine.path, manager::engine_working_dir(manager_working_dir, workflow.id));
    if (fetch_result) {
      std::cout << fetch_result.value();
    }

    return fetch_result.has_value();
  }

  CLI::App *register_workflow_log_path(CLI::App *parent, LogOptions &options) noexcept {
    auto cmd = parent->add_subcommand("log_path", "Get the path to the log file for the given workflow execution");
    cmd->add_option("id", options.workflow_id, "ID of the workflow execution")->required();

    return cmd;
  }

  bool workflow_log_path(const std::filesystem::path &manager_working_dir, const LogOptions &options) noexcept {
    const auto result = prepare_engine_call(manager_working_dir, options.workflow_id);
    if (!result) {
      return false;
    }

    const auto [engine, workflow] = result.value();

    const auto fetch_result = engine_cli::log_path(engine.path, manager::engine_working_dir(manager_working_dir, workflow.id));
    if (fetch_result) {
      std::cout << fetch_result.value() << '\n';
    }

    return fetch_result.has_value();
  }

  CLI::App *register_tree(CLI::App *parent, TreeOptions &options) noexcept {
    auto cmd = parent->add_subcommand("tree", "Print information about the nodes and their parent relations which can be used to understand the tree structure of a workflow with branches and branch parents.");
    cmd->add_option("id", options.workflow_id, "ID of the workflow execution")->required();

    return cmd;
  }

  bool tree(const std::filesystem::path &manager_working_dir, const TreeOptions &options) noexcept {
    const auto result = prepare_engine_call(manager_working_dir, options.workflow_id);
    if (!result) {
      return false;
    }

    const auto [engine, workflow] = result.value();

    const auto fetch_result = engine_cli::tree(engine.path, manager::engine_working_dir(manager_working_dir, workflow.id));
    if (fetch_result) {
      std::cout << fetch_result.value();
    }

    return fetch_result.has_value();
  }

  CLI::App *register_tree_path(CLI::App *parent, TreeOptions &options) noexcept {
    auto cmd = parent->add_subcommand("tree_path", "Get the path to the file containing the tree status information for the given workflow execution");
    cmd->add_option("id", options.workflow_id, "ID of the workflow execution")->required();

    return cmd;
  }

  bool tree_path(const std::filesystem::path &manager_working_dir, const TreeOptions &options) noexcept {
    const auto result = prepare_engine_call(manager_working_dir, options.workflow_id);
    if (!result) {
      return false;
    }

    const auto [engine, workflow] = result.value();

    const auto fetch_result = engine_cli::tree_path(engine.path, manager::engine_working_dir(manager_working_dir, workflow.id));
    if (fetch_result) {
      std::cout << fetch_result.value() << '\n';
    }

    return fetch_result.has_value();
  }

  CLI::App *register_workflow_artifacts(CLI::App *parent, ArtifactsOptions &options) noexcept {
    auto cmd = parent->add_subcommand("shortcuts", "Get a list of shortcuts for the given workflow execution");
    cmd->alias("artifacts");

    cmd->add_option("id", options.workflow_id, "ID of the workflow execution")->required();

    return cmd;
  }

  bool list_workflow_artifacts(const std::filesystem::path &manager_working_dir, const ArtifactsOptions &options) noexcept {
    const auto result = prepare_engine_call(manager_working_dir, options.workflow_id);
    if (!result) {
      return false;
    }

    const auto [engine, workflow] = result.value();

    const auto fetch_result = engine_cli::artifacts(engine.path, manager::engine_working_dir(manager_working_dir, workflow.id));
    if (fetch_result) {
      std::cout << fetch_result.value();
    }

    return fetch_result.has_value();
  }

  CLI::App *register_list(CLI::App *parent) noexcept {
    return parent->add_subcommand("list", "List workflow executions or available workflow engines");
  }

  CLI::App *register_list_engines(CLI::App *parent) noexcept {
    auto cmd = parent->add_subcommand("engines", "List available process engines");
    return cmd;
  }

  bool list_engines(const std::filesystem::path &manager_working_dir) noexcept {
    const auto engines_path = manager_working_dir / config::DEFAULT_ENGINE_FILENAME;

    const auto result = manager::load_engines(engines_path);
    if (!result) {
      std::cerr << "Could not load engines from '" << engines_path.string() << "'\n";
      return false;
    }

    std::cout << nlohmann::json(result->first).dump(2) << '\n';

    return true;
  }

  CLI::App *register_list_workflows(CLI::App *parent) noexcept {
    return parent->add_subcommand("workflows", "List workflow executions and execution history");
  }

  bool list_workflows(const std::filesystem::path &working_dir) noexcept {
    const auto engines_path = working_dir / config::DEFAULT_ENGINE_FILENAME;
    const auto loaded_engines = manager::load_engines(engines_path);

    auto db = manager::open_db(working_dir);
    if (!db) {
      return false;
    }

    if (loaded_engines) {
      // Fetch the status of all running engines.
      const auto unfinished_workflows = db->find_unfinished_workflows();

      // Try to update the status of the running engines.
      for (const auto &workflow : unfinished_workflows) {
        const auto engine = manager::find_engine_by_name(loaded_engines->first, workflow.engine);
        if (!engine) {
          continue;
        }

        if (!manager::update_workflow(workflow.id, engine->path, manager::engine_working_dir(working_dir, workflow.id), db.value())) {
          std::cerr << "Could not update status of workflow '" << workflow.id << "'\n";
        }
      }
    } else {
      std::cerr << "Running workflows cannot be updated because the engines cannot be loaded from '" << engines_path.string() << "'.\n";
    }

    const auto workflows = db->find_workflows();

    std::vector<nlohmann::json> result{};

    for (const auto &workflow : workflows) {
      result.push_back(workflow);
    }

    std::cout << nlohmann::json(result).dump(2) << '\n';

    return true;
  }

  CLI::App *register_status_workflow(CLI::App *parent, StatusOptions &options) noexcept {
    auto cmd = parent->add_subcommand("status", "Print status of the workflow execution with the given id");

    cmd->add_option("workflowID", options.workflow_id, "Id of the workflow execution")->required();
    cmd->add_flag("-f,--force-refresh", options.force_refresh, "Force a refresh by requesting the status from the responsible process engine. If not set, the status will only be refreshed for non-final states like 'running' or 'needs interaction'.");

    return cmd;
  }

  bool status(const std::filesystem::path &manager_working_dir, const StatusOptions &options) noexcept {
    auto db = manager::open_db(manager_working_dir);
    if (!db) {
      return false;
    }

    auto workflow = db->get_workflow_by_id(options.workflow_id);
    if (!workflow) {
      std::cerr << "Could not fetch workflow status\n";
      return false;
    }

    if (options.force_refresh || !workflow->has_finished()) {
      const auto loaded_engines = manager::load_engines(manager_working_dir / config::DEFAULT_ENGINE_FILENAME);
      if (!loaded_engines) {
        std::cerr << "Could not load engines\n";
        return false;
      }

      const auto engine = manager::find_engine_by_name(loaded_engines->first, workflow->engine);
      if (!engine) {
        std::cerr << "Engine '" << workflow->engine << "' could not be found\n";
        return false;
      }

      const auto engine_working_dir = manager::engine_working_dir(manager_working_dir, options.workflow_id);

      workflow = manager::update_workflow(options.workflow_id, engine->path, engine_working_dir, db.value());

      if (!workflow) {
        std::cerr << "Could not fetch new status workflow\n";
        return false;
      }
    }

    std::cout << nlohmann::json(workflow.value()).dump(2) << '\n';

    return true;
  }
} // namespace process_manager::cli