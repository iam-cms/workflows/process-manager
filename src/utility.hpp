#pragma once

#include <chrono>
#include <filesystem>
#include <optional>
#include <string>
#include <vector>

namespace process_manager {
  [[nodiscard]] std::optional<std::string> read_file_as_string(const std::filesystem::path &path) noexcept;
  [[nodiscard]] bool write_file(std::string_view content, const std::filesystem::path &path) noexcept;

  [[nodiscard]] std::filesystem::path home_directory() noexcept;

  [[nodiscard]] int to_unix_timestamp_seconds(const std::chrono::system_clock::time_point &time_point) noexcept;
  std::string &to_lower(std::string &str) noexcept;

  [[nodiscard]] std::chrono::system_clock::time_point parse_old_engine_time_format(std::string_view datetime) noexcept;
  [[nodiscard]] std::string to_old_engine_time_format(const std::chrono::system_clock::time_point &time_point) noexcept;

  template<typename T>
  [[nodiscard]] std::string join(const std::vector<T> &values, const std::string &delimiter) noexcept {
    std::ostringstream os{};

    for (auto iter = values.cbegin(); iter != values.cend(); ++iter) {
      os << *iter << delimiter;
    }

    auto result = os.str();
    result.erase(result.size() - delimiter.size());

    return result;
  }
} // namespace process_manager