#include "database.hpp"

#include "utility.hpp"

namespace {
  using namespace process_manager;

  struct SQL final {
    static constexpr std::string_view insert_workflow{"INSERT INTO workflows (engine, file, state) VALUES (?, ?, ?) RETURNING *;"};
    static constexpr std::string_view update_workflow{"UPDATE workflows SET state = ?,  node_count = ?, node_finished_count = ?, node_finished_in_loop_count = ?, started_at = ?, finished_at = ? WHERE id = ?;"};
    static constexpr std::string_view find_workflows_by_state{"SELECT * FROM workflows WHERE state = ?;"};
    static constexpr std::string_view find_workflows{"SELECT * FROM workflows;"};
    static constexpr std::string_view get_workflow_by_id{"SELECT * FROM workflows WHERE id = ?;"};
  };

  void reset_statement(sqlite3_stmt *stmt) noexcept {
    sqlite3_reset(stmt);
    sqlite3_clear_bindings(stmt);
  }

  [[nodiscard]] bool truncate_table(sqlite3 *db, std::string_view table) noexcept {

    std::stringstream delete_sql{};
    delete_sql << "DELETE FROM " << table << ";";

    std::stringstream reset_sql{};
    reset_sql << "UPDATE sqlite_sequence SET seq = 0 WHERE name = '" << table << "';";

    if (sqlite3_exec(db, delete_sql.str().c_str(), 0, 0, nullptr) != SQLITE_OK) {
      return false;
    }

    if (sqlite3_exec(db, reset_sql.str().c_str(), 0, 0, nullptr) != SQLITE_OK) {
      return false;
    }

    return true;
  }

  [[nodiscard]] bool create_workflow_table(sqlite3 *db) noexcept {
    const char *table = R"(
        CREATE TABLE IF NOT EXISTS workflows (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            engine TEXT,
            file TEXT,
            state TEXT,
            node_count INTEGER,
            node_finished_count INTEGER,
            node_finished_in_loop_count INTEGER,
            started_at INTEGER,
            finished_at INTEGER
        );
    )";

    return sqlite3_exec(db, table, 0, 0, nullptr) == SQLITE_OK;
  }

  [[nodiscard]] Workflow map_workflow(sqlite3_stmt *stmt) noexcept {
    return Workflow{
      .id = sqlite3_column_int(stmt, 0),
      .engine = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 1)),
      .file = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 2)),
      .state = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 3)),
      .node_count = sqlite3_column_int(stmt, 4),
      .node_finished_count = sqlite3_column_int(stmt, 5),
      .node_finished_in_loop_count = sqlite3_column_int(stmt, 6),
      .started_at = std::chrono::system_clock::from_time_t(std::time_t{sqlite3_column_int(stmt, 7)}),
      .finished_at = std::chrono::system_clock::from_time_t(std::time_t{sqlite3_column_int(stmt, 8)}),
    };
  }

  [[nodiscard]] std::vector<Workflow> map_workflows(sqlite3_stmt *stmt) noexcept {
    std::vector<Workflow> result{};

    while (sqlite3_step(stmt) == SQLITE_ROW) {
      result.push_back(map_workflow(stmt));
    }

    return result;
  }

  [[nodiscard]] std::vector<Workflow> find_workflows_by_state(sqlite3_stmt *stmt, std::string_view status) noexcept {
    auto r = sqlite3_bind_text(stmt, 1, status.data(), -1, SQLITE_STATIC);
    return map_workflows(stmt);
  }
} // namespace

namespace process_manager {
  Database::Database(Database &&other) noexcept {
    *this = std::move(other);
  }

  Database &Database::operator=(Database &&other) {
    db_ = std::exchange(other.db_, nullptr);
    stmts_ = std::exchange(other.stmts_, {});

    return *this;
  }

  Database::~Database() {
    close();
  }

  bool Database::open(const std::filesystem::path &db_path) noexcept {
    if (sqlite3_open(db_path.string().c_str(), &db_) != SQLITE_OK) {
      db_ = nullptr;
      return false;
    }

    if (!create_workflow_table(db_)) {
      close();
      return false;
    }

    bool success{true};

    success &= sqlite3_prepare_v2(db_, SQL::insert_workflow.data(), -1, &stmts_.insert_workflow, 0) == SQLITE_OK;
    success &= sqlite3_prepare_v2(db_, SQL::update_workflow.data(), -1, &stmts_.update_workflow, 0) == SQLITE_OK;
    success &= sqlite3_prepare_v2(db_, SQL::find_workflows_by_state.data(), -1, &stmts_.find_workflows_by_status, 0) == SQLITE_OK;
    success &= sqlite3_prepare_v2(db_, SQL::find_workflows.data(), -1, &stmts_.find_workflows, 0) == SQLITE_OK;
    success &= sqlite3_prepare_v2(db_, SQL::get_workflow_by_id.data(), -1, &stmts_.get_workflow_by_id, 0) == SQLITE_OK;

    if (!success) {
      close();
    }

    return success;
  }

  void Database::close() noexcept {
    sqlite3_close(db_);
    db_ = nullptr;
  }

  std::optional<Workflow> Database::insert_workflow(std::string_view engine_name, std::string_view workflow_file) noexcept {
    if (!is_open()) {
      return std::nullopt;
    }

    auto stmt = stmts_.insert_workflow;

    sqlite3_bind_text(stmt, 1, engine_name.data(), -1, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 2, workflow_file.data(), -1, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 3, "running", -1, SQLITE_STATIC);

    if (sqlite3_step(stmt) != SQLITE_ROW) {
      return std::nullopt;
    }

    const auto workflow = map_workflow(stmt);
    reset_statement(stmt);

    return workflow;
  }

  bool Database::update_workflow(const Workflow &workflow) noexcept {
    if (!is_open()) {
      return false;
    }

    auto stmt = stmts_.update_workflow;

    sqlite3_bind_text(stmt, 1, workflow.state.data(), -1, SQLITE_STATIC);
    sqlite3_bind_int(stmt, 2, workflow.node_count);
    sqlite3_bind_int(stmt, 3, workflow.node_finished_count);
    sqlite3_bind_int(stmt, 4, workflow.node_finished_in_loop_count);
    sqlite3_bind_int(stmt, 5, to_unix_timestamp_seconds(workflow.started_at));
    sqlite3_bind_int(stmt, 6, to_unix_timestamp_seconds(workflow.finished_at));
    sqlite3_bind_int(stmt, 7, workflow.id);

    if (sqlite3_step(stmt) != SQLITE_DONE) {
      return false;
    }

    reset_statement(stmt);
    return true;
  }

  bool Database::delete_all_workflows() noexcept {
    return truncate_table(db_, "workflows");
  }

  std::vector<Workflow> Database::find_unfinished_workflows() noexcept {
    if (!is_open()) {
      return {};
    }

    auto stmt = stmts_.find_workflows_by_status;

    const auto running = find_workflows_by_state(stmt, "running");
    reset_statement(stmt);

    const auto needs_interaction = find_workflows_by_state(stmt, "needs_interaction");
    reset_statement(stmt);

    std::vector<Workflow> result{};
    result.reserve(running.size() + needs_interaction.size());

    result.insert(result.end(), running.begin(), running.end());
    result.insert(result.end(), needs_interaction.begin(), needs_interaction.end());

    return result;
  }

  std::vector<Workflow> Database::find_workflows() noexcept {
    if (!is_open()) {
      return {};
    }

    auto stmt = stmts_.find_workflows;
    const auto workflows = map_workflows(stmt);

    reset_statement(stmt);
    return workflows;
  }

  std::optional<Workflow> Database::get_workflow_by_id(int id) noexcept {
    if (!is_open()) {
      return std::nullopt;
    }

    auto stmt = stmts_.get_workflow_by_id;

    sqlite3_bind_int(stmt, 1, id);

    if (sqlite3_step(stmt) != SQLITE_ROW) {
      return std::nullopt;
    }

    const auto workflow = map_workflow(stmt);
    reset_statement(stmt);

    return workflow;
  }

  bool Database::is_open() const noexcept {
    return db_ != nullptr;
  }
} // namespace process_manager