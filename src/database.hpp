#pragma once

#include "workflow.hpp"

#include <sqlite3.h>

#include <chrono>
#include <filesystem>
#include <optional>
#include <string>
#include <string_view>
#include <vector>

namespace process_manager {
  class Database final {
  public:
    Database() = default;
    Database(Database &&other) noexcept;
    ~Database();

    Database &operator=(Database &&other);

    [[nodiscard]] bool open(const std::filesystem::path &db_path) noexcept;
    void close() noexcept;
    [[nodiscard]] bool is_open() const noexcept;

    [[nodiscard]] std::optional<Workflow> insert_workflow(std::string_view engine_name, std::string_view workflow_file) noexcept;
    [[nodiscard]] bool update_workflow(const Workflow &workflow) noexcept;
    [[nodiscard]] bool delete_all_workflows() noexcept;

    [[nodiscard]] std::vector<Workflow> find_unfinished_workflows() noexcept;
    [[nodiscard]] std::vector<Workflow> find_workflows() noexcept;
    [[nodiscard]] std::optional<Workflow> get_workflow_by_id(int id) noexcept;

  private:
    sqlite3 *db_{};

    struct Statements final {
      sqlite3_stmt *insert_workflow{};
      sqlite3_stmt *update_workflow{};
      sqlite3_stmt *find_workflows_by_status{};
      sqlite3_stmt *find_workflows{};
      sqlite3_stmt *get_workflow_by_id{};
    };

    Statements stmts_{};
  };
} // namespace process_manager