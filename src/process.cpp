#include "process.hpp"

#include "utility.hpp"

#ifndef WIN32

#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#else

#include <stdio.h>
#include <tchar.h>

#define NOMINMAX
#include <windows.h>

#endif

#include <array>
#include <future>
#include <iostream>
#include <sstream>

namespace {
#ifndef WIN32
  std::string read_pipe(int pipe) {
    std::array<char, 256> buffer{};
    std::stringstream stream{};
    ssize_t bytes_read{};

    while ((bytes_read = read(pipe, buffer.data(), buffer.size())) > 0) {
      stream.write(buffer.data(), bytes_read);
    }

    return stream.str();
  }
#else
  std::string read_pipe(HANDLE pipe) {
    std::array<char, 256> buffer{};
    std::stringstream stream{};

    DWORD bytes_read{};
    HANDLE hParentStdOut = GetStdHandle(STD_OUTPUT_HANDLE);

    for (;;) {
      const auto success = ReadFile(pipe, buffer.data(), static_cast<DWORD>(buffer.size()), &bytes_read, NULL);
      if (!success || bytes_read == 0) {
        break;
      }

      stream.write(buffer.data(), bytes_read);
    }

    return stream.str();
  }

  std::string escape_arg(const std::string &arg, bool force) {
    // Original source: https://learn.microsoft.com/de-de/archive/blogs/twistylittlepassagesallalike/everyone-quotes-command-line-arguments-the-wrong-way

    std::string_view argument = arg;

    // Single quotes around the arg should not be used under windows.
    if (argument.front() == '\'' && argument.back() == '\'') {
      argument = argument.substr(1, argument.size() - 2);
    }

    std::string command_line{};

    // Unless we're told otherwise, don't quote unless we actually
    // need to do so --- hopefully avoid problems if programs won't
    // parse quotes properly
    if (force == false && argument.empty() == false && argument.find_first_of(" \t\n\v\"") == argument.npos) {
      command_line.append(argument);
    } else {
      const auto needs_quotes = argument.empty() || !(argument.front() == '"' && argument.back() == '"');

      if (needs_quotes) {
        command_line.push_back('"');
      }

      for (auto it = argument.begin();; ++it) {
        unsigned number_backslashes = 0;

        while (it != argument.end() && *it == '\\') {
          ++it;
          ++number_backslashes;
        }

        if (it == argument.end()) {
          // Escape all backslashes, but let the terminating
          // double quotation mark we add below be interpreted
          // as a metacharacter.
          command_line.append(number_backslashes * 2, '\\');
          break;
        } else if (*it == '"') {
          // Escape all backslashes and the following
          // double quotation mark.
          command_line.append(number_backslashes * 2 + 1, '\\');
          command_line.push_back(*it);
        } else {
          // Backslashes aren't special here.
          command_line.append(number_backslashes, '\\');
          command_line.push_back(*it);
        }
      }

      if (needs_quotes) {
        command_line.push_back('"');
      }
    }

    return command_line;
  }

  [[nodiscard]] std::string argv_to_commandline(const std::vector<std::string> &argv) noexcept {
    std::vector<std::string> escaped_args{};
    escaped_args.resize(argv.size());

    std::transform(argv.begin(), argv.end(), escaped_args.begin(), [](const std::string &arg) {
      return escape_arg(arg, false);
    });

    return process_manager::join(escaped_args, " ");
  }

#endif
} // namespace

namespace process_manager::process {
  Result run(const std::vector<std::string> &args, [[maybe_unused]] std::chrono::milliseconds timeout, std::stop_token terminate_token) noexcept {
#ifndef WIN32
    // TODO: Account for timeout.
    Result result{};

    int stdout_pipe[2];
    int stderr_pipe[2];

    if (pipe(stdout_pipe) != 0 || pipe(stderr_pipe) != 0) {
      result.status = -1;
      result.error_code = std::make_error_code(std::errc::broken_pipe);

      return result;
    }

    auto pid = fork();
    if (pid < 0) {
      close(stdout_pipe[0]);
      close(stdout_pipe[1]);

      close(stderr_pipe[0]);
      close(stderr_pipe[1]);

      result.status = pid;
      result.error_code = std::make_error_code(static_cast<std::errc>(errno));

      return result;
    }

    // Child process.
    if (pid == 0) {
      // Move to own process group.
      setpgid(0, 0);

      // Redirect the pipes to the stdout and stderr of the process.
      dup2(stdout_pipe[1], STDOUT_FILENO);
      dup2(stderr_pipe[1], STDERR_FILENO);

      // The FD's are duplicated with dup2 and can be closed.
      close(stdout_pipe[0]);
      close(stdout_pipe[1]);

      close(stderr_pipe[0]);
      close(stderr_pipe[1]);

      std::vector<const char *> argv{};
      // An additional nullptr argument at the end is needed for execvp to indicate the end.
      argv.resize(args.size() + 1);

      for (size_t i = 0; i < args.size(); ++i) {
        argv.at(i) = args.at(i).c_str();
      }

      result.status = execvp(argv.at(0), const_cast<char *const *>(argv.data()));
      result.error_code = std::make_error_code(static_cast<std::errc>(errno));

      return result;
    }

    std::stop_callback stop_callback(terminate_token, [pid]() {
      killpg(getpgid(pid), SIGKILL);
    });

    // In the parent process we don't want to write at the moment.
    close(stdout_pipe[1]);
    close(stderr_pipe[1]);

    // Start to tasks to read stdout and stderr in parallel to avoid stalls.
    auto stdout_task = std::async(std::launch::async, [&stdout_pipe]() { return read_pipe(stdout_pipe[0]); });
    auto stderror_task = std::async(std::launch::async, [&stderr_pipe]() { return read_pipe(stderr_pipe[0]); });

    const std::string output = stdout_task.get();
    const std::string error = stderror_task.get();

    // Finished reading.
    close(stdout_pipe[0]);
    close(stderr_pipe[0]);

    waitpid(pid, &result.status, 0);

    result.out = std::move(output);
    result.err = std::move(error);

    return result;

#else
    Result result{};

    HANDLE stdout_read{};
    HANDLE stdout_write{};

    HANDLE stderr_read{};
    HANDLE stderr_write{};

    const auto on_error = [&result, &stdout_read, &stdout_write, &stderr_read, &stderr_write]() {
      result.status = 1;
      result.error_code = std::make_error_code(static_cast<std::errc>(GetLastError()));
      return result;
    };

    SECURITY_ATTRIBUTES security_attributes{};
    security_attributes.nLength = sizeof(SECURITY_ATTRIBUTES);
    security_attributes.bInheritHandle = TRUE;
    security_attributes.lpSecurityDescriptor = NULL;

    if (!CreatePipe(&stdout_read, &stdout_write, &security_attributes, 0)) {
      return on_error();
    }

    if (!CreatePipe(&stderr_read, &stderr_write, &security_attributes, 0)) {
      return on_error();
    }

    // Create a job object so that all started processes are in the same “group” in order to end them all together.
    auto job = CreateJobObject(NULL, NULL);
    if (job == NULL) {
      return on_error();
    }

    // Terminate all processes in the job when the job is closed.
    JOBOBJECT_EXTENDED_LIMIT_INFORMATION job_info{};
    job_info.BasicLimitInformation.LimitFlags = JOB_OBJECT_LIMIT_KILL_ON_JOB_CLOSE;
    SetInformationJobObject(job, JobObjectExtendedLimitInformation, &job_info, sizeof(job_info));

    STARTUPINFO startup_info{};
    startup_info.cb = sizeof(startup_info);
    startup_info.hStdOutput = stdout_write;
    startup_info.hStdError = stderr_write;
    startup_info.hStdInput = NULL;
    startup_info.dwFlags |= STARTF_USESTDHANDLES;

    PROCESS_INFORMATION process_info{};

    auto program = argv_to_commandline(args);

    // The process is started in a suspended state to assign it to the job.
    if (!CreateProcess(NULL, program.data(), NULL, NULL, TRUE, CREATE_SUSPENDED, NULL, NULL, &startup_info, &process_info)) {
      return on_error();
    }

    CloseHandle(stdout_write);
    CloseHandle(stderr_write);

    if (!AssignProcessToJobObject(job, process_info.hProcess)) {
      return on_error();
    }

    std::stop_callback stop_callback(terminate_token, [&job]() {
      // TODO: Use different mechanism to stop the process?
      // This way the process has no chance to cleanup it's resources.
      TerminateJobObject(job, EXIT_FAILURE);
    });

    // Start to tasks to read stdout and stderr in parallel to avoid stalls.
    auto stdout_task = std::async(std::launch::async, [&stdout_read]() { return read_pipe(stdout_read); });
    auto stderror_task = std::async(std::launch::async, [&stderr_read]() { return read_pipe(stderr_read); });

    // Process is ready to run.
    ResumeThread(process_info.hThread);

    // TODO: Use timeout?
    WaitForSingleObject(process_info.hProcess, INFINITE);

    DWORD exit_code{};
    if (GetExitCodeProcess(process_info.hProcess, &exit_code)) {
      result.status = exit_code;
    }

    result.out = stdout_task.get();
    result.err = stderror_task.get();

    CloseHandle(job);

    CloseHandle(process_info.hProcess);
    CloseHandle(process_info.hThread);

    CloseHandle(stdout_read);
    CloseHandle(stderr_read);

    return result;

#endif
  }

  bool run_detached(const std::vector<std::string> &args) noexcept {
#ifndef WIN32
    const auto pid = fork();

    if (pid < 0) {
      return false;
    }

    if (pid > 0) {
      return true;
    }

    if (setsid() < 0) {
      return false;
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    std::vector<const char *> argv{};
    for (size_t i = 0; i < args.size(); ++i) {
      argv.push_back(args.at(i).c_str());
    }

    argv.emplace_back(nullptr);

    execvp(argv.at(0), const_cast<char *const *>(argv.data()));
    return false;

#else
    STARTUPINFO startup_info{};
    PROCESS_INFORMATION process_info{};

    auto program = argv_to_commandline(args);

    const auto success = CreateProcess(NULL, program.data(), NULL, NULL, FALSE, DETACHED_PROCESS | CREATE_NO_WINDOW, NULL, NULL, &startup_info, &process_info);

    CloseHandle(process_info.hProcess);
    CloseHandle(process_info.hThread);

    return success;
#endif
  }
} // namespace process_manager::process