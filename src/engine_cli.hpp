#pragma once

#include <filesystem>
#include <optional>
#include <string>
#include <string_view>
#include <vector>

namespace process_manager::engine_cli {
  [[nodiscard]] bool run(const std::filesystem::path &engine_path, const std::filesystem::path &engine_working_dir, const std::filesystem::path &workflow_file, bool no_color) noexcept;
  [[nodiscard]] bool resume(const std::filesystem::path &engine_path, const std::filesystem::path &engine_working_dir);
  [[nodiscard]] bool cancel(const std::filesystem::path &engine_path, const std::filesystem::path &engine_working_dir);

  [[nodiscard]] std::optional<std::string> log(const std::filesystem::path &engine_path, const std::filesystem::path &engine_working_dir) noexcept;
  [[nodiscard]] std::optional<std::string> log_path(const std::filesystem::path &engine_path, const std::filesystem::path &engine_working_dir) noexcept;

  [[nodiscard]] std::optional<std::string> tree(const std::filesystem::path &engine_path, const std::filesystem::path &engine_working_dir) noexcept;
  [[nodiscard]] std::optional<std::string> tree_path(const std::filesystem::path &engine_path, const std::filesystem::path &engine_working_dir) noexcept;

  [[nodiscard]] std::optional<std::string> interactions(const std::filesystem::path &engine_path, const std::filesystem::path &engine_working_dir) noexcept;
  [[nodiscard]] bool input(const std::filesystem::path &engine_path, const std::filesystem::path &engine_working_dir, std::string interaction_id, std::string value) noexcept;

  [[nodiscard]] std::optional<std::string> status(const std::filesystem::path &engine_path, const std::filesystem::path &engine_working_dir) noexcept;
  [[nodiscard]] std::optional<std::string> artifacts(const std::filesystem::path &engine_path, const std::filesystem::path &engine_working_dir) noexcept;
} // namespace process_manager