#pragma once

#include <nlohmann/json.hpp>

#include <chrono>
#include <string>

namespace process_manager {
  struct Workflow final {
    int id{};
    std::string engine{};
    std::string file{};
    std::string state{};
    int node_count{};
    int node_finished_count{};
    // TODO: Use this?
    int node_finished_in_loop_count{};
    std::chrono::system_clock::time_point started_at{};
    std::chrono::system_clock::time_point finished_at{};

    [[nodiscard]] bool has_finished() const noexcept {
      // TODO: More states?
      return state == "error" || state == "finished";
    }
  };


  void to_json(nlohmann::json &j, const Workflow &workflow);

} // namespace process_manager