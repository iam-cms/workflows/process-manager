#pragma once

#include "database.hpp"

#include <nlohmann/json.hpp>

#include <chrono>
#include <filesystem>
#include <optional>
#include <string>
#include <vector>

namespace process_manager {
  struct Engine final {
    std::string name{};
    std::string path{};
  };

  using Engines = std::vector<Engine>;

  NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(Engine, name, path)
} // namespace process_manager

namespace process_manager::manager {
  [[nodiscard]] nlohmann::json default_engines_json() noexcept;

  [[nodiscard]] std::filesystem::path default_working_dir() noexcept;
  [[nodiscard]] std::optional<std::filesystem::path> setup_working_dir(const std::filesystem::path &path) noexcept;

  [[nodiscard]] std::filesystem::path engine_working_dir(const std::filesystem::path &manager_working_dir, int workflow_id) noexcept;
  [[nodiscard]] std::optional<std::filesystem::path> setup_engine_working_dir(const std::filesystem::path &manager_working_dir, int workflow_id) noexcept;

  [[nodiscard]] std::optional<Engine> find_engine_by_name(const Engines &engines, std::string_view name) noexcept;

  [[nodiscard]] std::optional<Engine> select_engine(const Engines &engines, std::string_view preferred_engine_name, std::string_view default_engine_name) noexcept;
  [[nodiscard]] std::optional<std::pair<Engines, std::string>> load_engines(const std::filesystem::path &path) noexcept;

  [[nodiscard]] std::optional<Workflow> update_workflow(int id, const std::filesystem::path &engine_path, const std::filesystem::path &engine_working_dir, Database &db) noexcept;

  [[nodiscard]] std::optional<Database> open_db(const std::filesystem::path &working_dir) noexcept;
} // namespace process_manager::manager