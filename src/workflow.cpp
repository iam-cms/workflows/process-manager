#include "workflow.hpp"

#include "utility.hpp"

namespace process_manager {

  void to_json(nlohmann::json &j, const Workflow &workflow) {
    // TODO: Uppercase really needed?
    auto state = workflow.state;
    state[0] = toupper(state[0]);

    j["id"] = workflow.id;
    j["processEngine"] = workflow.engine;
    j["fileName"] = workflow.file;
    j["state"] = state;

    // TODO: Correct names?
    // TODO: Do not use this format.
    j["startDateTime"] = workflow.started_at.time_since_epoch().count() == 0 ? "" : to_old_engine_time_format(workflow.started_at);
    j["endDateTime"] = workflow.finished_at.time_since_epoch().count() == 0 ? "" : to_old_engine_time_format(workflow.finished_at);

    // TODO: Implement.
    // TODO: Correct names?
    j["nodesTotal"] = workflow.node_count;
    j["nodesProcessed"] = workflow.node_finished_count;
    j["nodesProcessedInLoops"] = workflow.node_finished_in_loop_count;

    // TODO: What is this used for?
    j["iterations"] = 0;
  }
} // namespace process_manager