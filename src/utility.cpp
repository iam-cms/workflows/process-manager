#include "utility.hpp"

#include <algorithm>
#include <ctime>
#include <fstream>
#include <sstream>

#ifndef WIN32
#include <pwd.h>
#include <unistd.h>
#endif

namespace process_manager {
  std::optional<std::string> read_file_as_string(const std::filesystem::path &path) noexcept {
    std::error_code error_code{};
    if (!std::filesystem::exists(path, error_code)) {
      return std::nullopt;
    }

    const auto file_size = std::filesystem::file_size(path, error_code);
    if (error_code) {
      return std::nullopt;
    }

    std::string content{};
    content.resize(file_size);

    std::ifstream stream(path, std::ios::in);
    if (!stream.is_open()) {
      return std::nullopt;
    }

    stream.read(content.data(), content.size());
    stream.close();

    return content;
  }

  bool write_file(std::string_view content, const std::filesystem::path &path) noexcept {
    std::ofstream stream(path, std::ios::out);
    if (!stream.is_open()) {
      return false;
    }

    stream.write(content.data(), content.size());
    return !stream.fail();
  }

  std::filesystem::path home_directory() noexcept {
#ifndef WIN32
    if (const auto home = std::getenv("HOME")) {
      return std::filesystem::path(home);
    }
    
    return std::filesystem::path(getpwuid(getuid())->pw_dir);
#else
    // TODO: Check if it works.
    const auto home_dir = std::filesystem::path(std::getenv("HOMEDRIVE")) / std::filesystem::path(std::getenv("HOMEPATH"));
    return home_dir;
#endif
  }

  int to_unix_timestamp_seconds(const std::chrono::system_clock::time_point &time_point) noexcept {
    return std::chrono::duration_cast<std::chrono::seconds>(time_point.time_since_epoch()).count();
  }

  std::string &to_lower(std::string &str) noexcept {
    std::transform(str.begin(), str.end(), str.begin(), [](unsigned char c) { return std::tolower(c); });
    return str;
  }

  std::chrono::system_clock::time_point parse_old_engine_time_format(std::string_view datetime) noexcept {
    if (datetime.empty()) {
      return std::chrono::system_clock::time_point{};
    }

    std::tm tm{};
    char delim;

    std::istringstream ss{std::string(datetime)};

    ss >> tm.tm_hour >> delim >> tm.tm_min >> delim >> tm.tm_sec;
    ss >> tm.tm_mday >> delim >> tm.tm_mon >> delim >> tm.tm_year;

    tm.tm_mon -= 1;
    tm.tm_year -= 1900;

    std::time_t time = std::mktime(&tm);
    return std::chrono::system_clock::from_time_t(time);
  }

  std::string to_old_engine_time_format(const std::chrono::system_clock::time_point &time_point) noexcept {
    const auto time = std::chrono::system_clock::to_time_t(time_point);
    std::tm *tm = std::localtime(&time);

    std::stringstream result{};
    result << std::put_time(tm, "%H:%M:%S %d.%m.%Y");

    return result.str();
  }
} // namespace process_manager