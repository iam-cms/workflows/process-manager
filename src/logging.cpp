#include "logging.hpp"

#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_sinks.h>

namespace process_manager::logging {
  std::shared_ptr<spdlog::logger> setup(const std::filesystem::path &manager_working_dir) noexcept {
    auto logger = spdlog::default_logger();
    logger->sinks().clear();

    logger->sinks().push_back(std::make_shared<spdlog::sinks::basic_file_sink_mt>((manager_working_dir / "log.txt").string()));
    // logger->sinks().push_back(std::make_shared<spdlog::sinks::stderr_sink_mt>());

#ifndef NDEBUG
    logger->set_level(spdlog::level::debug);
#endif

    return logger;
  }

  std::shared_ptr<spdlog::logger> default_logger() noexcept {
    return spdlog::default_logger();
  }
} // namespace process_manager::logging