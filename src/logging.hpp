#pragma once

#include <spdlog/spdlog.h>

#include <filesystem>

namespace process_manager::logging {
  std::shared_ptr<spdlog::logger> setup(const std::filesystem::path &manager_working_dir) noexcept;
  std::shared_ptr<spdlog::logger> default_logger() noexcept;
} // namespace process_manager::logging