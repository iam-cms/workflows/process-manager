/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "cli.hpp"
#include "logging.hpp"
#include "manager.hpp"
#include "process.hpp"

#include <CLI/CLI.hpp>

using namespace std::chrono_literals;

using namespace process_manager;

int main(int argc, char **argv) {
  CLI::App app{"process_manager: A tool to manage workflow execution using compatible process engines."};
  app.set_version_flag("--version", PROCESS_MANAGER_VERSION);

  cli::CommonOptions common_options{};
  cli::register_commmon_options(&app, common_options);

  const auto list_cmd = cli::register_list(&app);
  const auto list_engines_cmd = cli::register_list_engines(list_cmd);
  const auto list_workflows_cmd = cli::register_list_workflows(list_cmd);

  cli::RunOptions run_options{};
  cli::ContinueOptions continue_options{};
  cli::CancelOptions cancel_options{};

  const auto run_cmd = cli::register_run_workflow(&app, run_options);
  const auto continue_cmd = cli::register_continue_workflow(&app, continue_options);
  const auto cancel_cmd = cli::register_cancel_workflow(&app, cancel_options);

  cli::InteractionOptions interaction_options{};
  cli::InputOptions input_options{};

  const auto interaction_cmd = cli::register_list_interactions(&app, interaction_options);
  const auto input_cmd = cli::register_add_input(&app, input_options);

  cli::StatusOptions status_options{};
  cli::ArtifactsOptions artifacts_options{};

  const auto status_cmd = cli::register_status_workflow(&app, status_options);
  const auto artifacts_cmd = cli::register_workflow_artifacts(&app, artifacts_options);

  cli::LogOptions log_options{};

  const auto log_cmd = cli::register_workflow_log(&app, log_options);
  const auto log_path_cmd = cli::register_workflow_log_path(&app, log_options);

  cli::TreeOptions tree_options{};

  const auto tree_cmd = cli::register_tree(&app, tree_options);
  const auto tree_path_cmd = cli::register_tree_path(&app, tree_options);

  CLI11_PARSE(app, argc, argv);
  try {
    app.parse(argc, argv);
  } catch (const CLI::ParseError &e) {
    return app.exit(e);
  }

  const auto working_dir = manager::setup_working_dir(common_options.working_dir);
  if (!working_dir) {
    std::cerr << "Failed to setup working directory\n";
    return 1;
  }

  auto logger = logging::setup(working_dir.value());

  try {
    bool result{true};

    if (*list_engines_cmd) {
      result &= cli::list_engines(working_dir.value());
    } else if (*list_workflows_cmd) {
      result &= cli::list_workflows(working_dir.value());
    } else if (*run_cmd) {
      result &= cli::run_workflow(working_dir.value(), run_options);
    } else if (*continue_cmd) {
      result &= cli::continue_workflow(working_dir.value(), continue_options);
    } else if (*cancel_cmd) {
      result &= cli::cancel_workflow(working_dir.value(), cancel_options);
    } else if (*interaction_cmd) {
      result &= cli::list_interactions(working_dir.value(), interaction_options);
    } else if (*input_cmd) {
      result &= cli::add_input(working_dir.value(), input_options);
    } else if (*status_cmd) {
      result &= cli::status(working_dir.value(), status_options);
    } else if (*artifacts_cmd) {
      result &= cli::list_workflow_artifacts(working_dir.value(), artifacts_options);
    } else if (*log_cmd) {
      result &= cli::workflow_log(working_dir.value(), log_options);
    } else if (*log_path_cmd) {
      result &= cli::workflow_log_path(working_dir.value(), log_options);
    } else if (*tree_cmd) {
      result &= cli::tree(working_dir.value(), tree_options);
    } else if (*tree_path_cmd) {
      result &= cli::tree_path(working_dir.value(), tree_options);
    }

    return result ? 0 : 1;
  } catch (const std::exception &exception) {
    logger->critical(exception.what());
    return 1;
  }

  return 0;
}
