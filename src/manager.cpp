#include "manager.hpp"

#include "config.hpp"
#include "engine_cli.hpp"
#include "logging.hpp"
#include "utility.hpp"

#include <iostream>
#include <sstream>

namespace {
  using namespace process_manager;

  [[nodiscard]] bool create_dir(const std::filesystem::path &path) noexcept {
    std::error_code error_code{};
    return std::filesystem::exists(path, error_code) ? true : std::filesystem::create_directories(path, error_code);
  }
} // namespace

namespace process_manager::manager {
  nlohmann::json default_engines_json() noexcept {
    nlohmann::json engines_json{};

    engines_json["engines"] = std::vector<nlohmann::json>{
      {
        {"name", "Process-Engine"},
        {"path", "process_engine"},
      },
    };

    engines_json["default_engine"] = "Process-Engine";

    return engines_json;
  }

  std::filesystem::path default_working_dir() noexcept {
    return home_directory() / ".process_manager";
  }

  std::optional<std::filesystem::path> setup_working_dir(const std::filesystem::path &path) noexcept {
    const auto working_dir = path.empty() ? default_working_dir() : path;

    if (!create_dir(working_dir)) {
      return std::nullopt;
    }

    if (!create_dir(working_dir / "workflows")) {
      return std::nullopt;
    }

    const auto engines_path = working_dir / config::DEFAULT_ENGINE_FILENAME;

    std::error_code error_code{};
    if (!std::filesystem::exists(engines_path, error_code)) {
      if (!write_file(default_engines_json().dump(2), engines_path)) {
        return std::nullopt;
      }
    }

    return working_dir;
  }

  std::filesystem::path engine_working_dir(const std::filesystem::path &manager_working_dir, int workflow_id) noexcept {
    return manager_working_dir / "workflows" / std::to_string(workflow_id);
  }

  std::optional<std::filesystem::path> setup_engine_working_dir(const std::filesystem::path &manager_working_dir, int workflow_id) noexcept {
    const auto path = engine_working_dir(manager_working_dir, workflow_id);
    return create_dir(path) ? std::optional<std::filesystem::path>(path) : std::nullopt;
  }

  std::optional<std::pair<Engines, std::string>> load_engines(const std::filesystem::path &path) noexcept {
    const auto engine_file = read_file_as_string(path);
    if (!engine_file) {
      return std::nullopt;
    }

    if (!nlohmann::json::accept(engine_file.value())) {
      return std::nullopt;
    }

    const auto engine_json = nlohmann::json::parse(engine_file.value());
    return std::pair<Engines, std::string>{engine_json["engines"].get<Engines>(), engine_json["default_engine"].get<std::string>()};
  }

  std::optional<Workflow> update_workflow(int id, const std::filesystem::path &engine_path, const std::filesystem::path &engine_working_dir, Database &db) noexcept {
    auto logger = logging::default_logger();

    const auto status = engine_cli::status(engine_path, engine_working_dir);
    if (!status) {
      logger->error("Could not fetch status from '{}' for working dir '{}'", engine_path.string(), engine_working_dir.string());
      return std::nullopt;
    }

    if (!nlohmann::json::accept(status.value())) {
      logger->error("The engine '{}' could not deliver valid JSON for the status", engine_path.string());
      return std::nullopt;
    }

    const auto status_json = nlohmann::json::parse(status.value());

    const auto &started_at_json = status_json["startDateTime"];
    const auto &finished_at_json = status_json["endDateTime"];

    // The old format is just a string with time and date.
    const auto get_timestamp = [](const nlohmann::json &json) {
      return json.is_number_integer() ? std::chrono::system_clock::time_point(std::chrono::seconds(json.get<int>())) : parse_old_engine_time_format(json.get<std::string_view>());
    };

    auto state = status_json["state"].get<std::string>();
    to_lower(state);

    Workflow workflow{};
    workflow.id = id;
    workflow.state = state;
    workflow.started_at = get_timestamp(started_at_json);
    workflow.finished_at = get_timestamp(finished_at_json);
    workflow.node_count = status_json["nodesTotal"];
    workflow.node_finished_count = status_json["nodesProcessed"];
    workflow.node_finished_in_loop_count = status_json["nodesProcessedInLoops"];

    if (!db.update_workflow(workflow)) {
      logger->error("Could not update DB for engine '{}' in working dir '{}'", engine_path.string(), engine_working_dir.string());
      return std::nullopt;
    }

    return workflow;
  }

  std::optional<Engine> find_engine_by_name(const Engines &engines, std::string_view name) noexcept {
    const auto iter = std::find_if(engines.begin(), engines.end(), [&name](const auto &engine) {
      return engine.name == name;
    });

    return iter != engines.end() ? std::optional<Engine>(*iter) : std::nullopt;
  }

  std::optional<Engine> select_engine(const Engines &engines, std::string_view preferred_engine_name, std::string_view default_engine_name) noexcept {
    // Try to find the user specified engine.
    auto engine = find_engine_by_name(engines, preferred_engine_name);
    if (engine) {
      return engine.value();
    }

    // The custom engine could not be found or does not exists, try to find the default engine.
    const auto default_engine = find_engine_by_name(engines, default_engine_name);
    if (default_engine) {
      return default_engine.value();
    }

    return std::nullopt;
  }

  std::optional<Database> open_db(const std::filesystem::path &working_dir) noexcept {
    Database db{};
    if (!db.open(working_dir / "manager.db")) {
      return std::nullopt;
    }

    return db;
  }
} // namespace process_manager::manager
