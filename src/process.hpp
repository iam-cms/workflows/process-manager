/* Copyright 2021 Karlsruhe Institute of Technology
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License. */

#pragma once

#include <chrono>
#include <stop_token>
#include <string>
#include <system_error>
#include <vector>

namespace process_manager::process {
  struct Result {
    int status{};
    std::error_code error_code{};

    // TODO: Rename to a usefull name, but not stdout and stderr because these are already defined names under windows.
    std::string out{};
    std::string err{};
  };

  [[nodiscard]] Result run(const std::vector<std::string> &args, std::chrono::milliseconds timeout, std::stop_token terminate_token) noexcept;
  [[nodiscard]] bool run_detached(const std::vector<std::string> &args) noexcept;
} // namespace process_manager::process