#pragma once

#include <CLI/CLI.hpp>

#include <string>

namespace process_manager::cli {
  struct CommonOptions final {
    std::string working_dir{};
  };

  struct RunOptions final {
    std::string engine{};
    std::string workflow_file{};
    bool no_color{};
  };

  struct CancelOptions final {
    int workflow_id{};
  };

  struct ContinueOptions final {
    int workflow_id{};
  };

  struct StatusOptions final {
    int workflow_id{};
    bool force_refresh{};
  };

  struct InteractionOptions final {
    int workflow_id{};
  };

  struct InputOptions final {
    int workflow_id{};
    std::string interaction_id{};
    std::string value{};
  };

  struct LogOptions final {
    int workflow_id{};
  };

  struct TreeOptions final {
    int workflow_id{};
  };

  struct ArtifactsOptions final {
    int workflow_id{};
  };

  void register_commmon_options(CLI::App *parent, CommonOptions &options) noexcept;

  [[nodiscard]] CLI::App *register_run_workflow(CLI::App *parent, RunOptions &options) noexcept;
  [[nodiscard]] bool run_workflow(const std::filesystem::path &manager_working_dir, const RunOptions &options) noexcept;

  [[nodiscard]] CLI::App *register_cancel_workflow(CLI::App *parent, CancelOptions &options) noexcept;
  [[nodiscard]] bool cancel_workflow(const std::filesystem::path &manager_working_dir, const CancelOptions &options) noexcept;

  [[nodiscard]] CLI::App *register_continue_workflow(CLI::App *parent, ContinueOptions &options) noexcept;
  [[nodiscard]] bool continue_workflow(const std::filesystem::path &manager_working_dir, const ContinueOptions &options) noexcept;

  [[nodiscard]] CLI::App *register_list_interactions(CLI::App *parent, InteractionOptions &options) noexcept;
  [[nodiscard]] bool list_interactions(const std::filesystem::path &manager_working_dir, const InteractionOptions &options) noexcept;

  [[nodiscard]] CLI::App *register_add_input(CLI::App *parent, InputOptions &options) noexcept;
  [[nodiscard]] bool add_input(const std::filesystem::path &manager_working_dir, const InputOptions &options) noexcept;

  [[nodiscard]] CLI::App *register_workflow_log(CLI::App *parent, LogOptions &options) noexcept;
  [[nodiscard]] bool workflow_log(const std::filesystem::path &manager_working_dir, const LogOptions &options) noexcept;

  [[nodiscard]] CLI::App *register_workflow_log_path(CLI::App *parent, LogOptions &options) noexcept;
  [[nodiscard]] bool workflow_log_path(const std::filesystem::path &manager_working_dir, const LogOptions &options) noexcept;

  [[nodiscard]] CLI::App *register_tree(CLI::App *parent, TreeOptions &options) noexcept;
  [[nodiscard]] bool tree(const std::filesystem::path &manager_working_dir, const TreeOptions &options) noexcept;

  [[nodiscard]] CLI::App *register_tree_path(CLI::App *parent, TreeOptions &options) noexcept;
  [[nodiscard]] bool tree_path(const std::filesystem::path &manager_working_dir, const TreeOptions &options) noexcept;

  [[nodiscard]] CLI::App *register_workflow_artifacts(CLI::App *parent, ArtifactsOptions &options) noexcept;
  [[nodiscard]] bool list_workflow_artifacts(const std::filesystem::path &manager_working_dir, const ArtifactsOptions &options) noexcept;

  [[nodiscard]] CLI::App *register_list(CLI::App *parent) noexcept;

  [[nodiscard]] CLI::App *register_list_engines(CLI::App *parent) noexcept;
  [[nodiscard]] bool list_engines(const std::filesystem::path &manager_working_dir) noexcept;

  [[nodiscard]] CLI::App *register_list_workflows(CLI::App *parent) noexcept;
  [[nodiscard]] bool list_workflows(const std::filesystem::path &working_dir) noexcept;

  [[nodiscard]] CLI::App *register_status_workflow(CLI::App *parent, StatusOptions &options) noexcept;
  [[nodiscard]] bool status(const std::filesystem::path &manager_working_dir, const StatusOptions &options) noexcept;
} // namespace process_manager::cli