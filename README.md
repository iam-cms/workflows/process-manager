# Process-Manager
A commandline application to manage workflow executions.

## How to Build
Build dependencies: cmake, C++ compiler
```
mkdir build
cmake -B build/ -S .
cmake --build build/
```

## Usage
```
process_manager: A tool to manage workflow execution using compatible process engines.
Usage: process_manager [OPTIONS] SUBCOMMAND

Options:
  -h,--help                   Print this help message and exit
  --version                   Display program version information and exit
  -p,--path TEXT              Base directory to execute in (application needs write permissions here). Defaults to $HOME/.process_manager

Subcommands:
  list                        List workflow executions or available workflow engines
  run, start                  Start a workflow from a .flow file
  continue                    Continue execution of a workflow with given id
  cancel                      Cancel execution of a workflow with given id
  interactions                Get interaction descriptions for given workflow execution
  input                       Provide input value for requested user interaction to the workflow engine
  status                      Print status of the workflow execution with the given id
  shortcuts, artifacts        Get a list of shortcuts for the given workflow execution
  log                         Get the log of given workflow execution
```

## Community projects

The following projects were created by users of process_manager:

- [kadi-workflow-execution](https://gitlab.com/YannickNoelStephanKuhn/kadi-workflow-execution): Helper scripts to demonstrate and run process_manager
